#!/usr/bin/env python3
#
#	Write a function that takes an integer argument N and finds the
#	"maximal divisor" --- that maximal number (other than N itself)
#	that N is divisible by
#

def max_divisor(N):
	for m in range(N, 0, -1):	# 0 will never be reached, but 1 will be
		n = (N // m) * m	# integer division always rounds down
		if n == N:
			return m
