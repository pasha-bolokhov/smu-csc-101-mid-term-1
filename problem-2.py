#!/usr/bin/env python3
#
#	Given the coefficients a, b, c of the quadratic equation
#	define the function which would return the greater (upper solution)
#

import math

def upper_root(a, b, c):

	descr = b**2 - 4.0 * a * c

	# check if there are solutions at all
	if descr < 0:
		return None

	# should have at least one solution
	x = (-b + math.sqrt(descr)) / (2.0 * a)
	return x
