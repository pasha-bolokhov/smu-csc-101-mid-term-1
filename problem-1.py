#!/usr/bin/env python3
#
#	Write a function that finds the medium number of three given numbers
#

def find_medium(n1, n2, n3):
#
# Notice that the "else" or "elif" statements are not needed
# as function necessarily "returns" and thus does not go anywhere
# below once an "if" condition has been met
#
	if n1 >= n2:
		if n2 >= n3:
			return n2
		if n1 >= n3:
			return n3
		return n1
#
# The code below can only be reached if n1 < n2
# Here go the same comparisons as above with n1 and n2 interchanged
# Again, no need in "else" or "elif" statements
#
	if n1 >= n3:
		return n1
	if n2 >= n3:
		return n3
	return n2
